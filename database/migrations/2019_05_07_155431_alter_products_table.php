<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//use DB;

class AlterProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('products', function ($table) {
			$table->double('amount', 8, 2)->after('slug'); 
            $table->string('description')->after('amount');
			$table->renameColumn('posted_by', 'created_by');
			
        });
        //DB::query("ALTER TABLE `ltest`.`users` CHANGE COLUMN `active` `active` tinyint(1) NOT NULL DEFAULT '1';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
               $table->dropColumn('like');
               $table->dropColumn('dislike');
        });
    }
}
